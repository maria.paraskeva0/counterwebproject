package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class CountTest {
  
   @Test
   public void test1() {
       assertEquals("MAX", Integer.MAX_VALUE, new Count().doSomething(anyInt(), 0));
   }

   @Test
   public void test2() {
       assertEquals("Divide", 2, new Count().doSomething(6, 3));
   }

}
